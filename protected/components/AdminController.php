<?php
class AdminController extends Controller
{
	public $layout = "//layouts/main";
	public $package = array(
		'basePath' => 'application.themes.admin',
		'baseUrl' => 'themes/admin/',
		'css' => array('css/bootstrap.min.css', 'css/demo_table_jui.css', 'css/style.css'),
		'js' => array('js/jquery.dataTables.min.js','js/script.js'),
		'depends' => array('jquery')
	);

	public function init()
	{
		parent::init();
		Yii::app()->theme = 'admin';
		Yii::app()->clientScript->addPackage('admin', $this->package)->registerPackage('admin');
		Yii::import('ext.imperavi-redactor-widget.ImperaviRedactorWidget');
	}
}