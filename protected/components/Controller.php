<?php

class Controller extends CController
{
	public $menu=array();
	public $breadcrumbs=array();

	protected   $error;
	protected   $result = array();
	protected $baseUrl;

	public function init()
	{
		$this->baseUrl = Yii::app()->getBaseUrl(true);
	}

	protected function give_the_result()
	{
		$r = array();
		if ($this->error) {
			$r['e'] = 1;
			$r['d'] = $this->error;
		}
		else {
			$r['e'] = 0;
			$r['r'] = $this->result;
		}
		echo json_encode($r);
	}

	protected function moveFile($filename, $user_id)
	{
		$path = YiiBase::getPathOfAlias('webroot') . "/storage/";

		$newdir = $path . $user_id;
		$oldfile = $path . "temp/$filename";

		if (file_exists($oldfile) && (is_dir($newdir) || mkdir($newdir, 0777))) {
			copy($oldfile, $newdir . "/$filename");
			unlink($oldfile);
		}
	}
}