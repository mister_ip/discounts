<?php
class PageController extends Controller
{
	public  $footer_slider;
	public  $categories;
	public $layout = "//layouts/main";
	public $path;
	public $package = array(
		'basePath' => 'application.themes.modern',
		'baseUrl' => 'themes/modern/',
		'css' => array( 'css/vmsite-ltr.css',
						'css/vmsite-rtl.css',
						'css/bootstrap.min.css',
						'css/bootstrap-responsive.min.css',
						'css/jquery.bxslider.css',
						'css/template.css',
						'css/style.css',
						'css/no-space.css',
						'css/omgmenu.css',
						'css/omgmenu_mobile.css'
				),
		'js' => array('js/jquery.bxslider.min.js', 'js/bootstrap.min.js', 'js/omgmenu.jq.js', 'js/script.js'),
		'depends'=>array('jquery', 'maps')
	);

	public function init()
	{
		parent::init();
		Yii::app()->clientScript->addPackage('main', $this->package)->registerPackage('main');
		$this->path =  YiiBase::getPathOfAlias('webroot.themes') . DIRECTORY_SEPARATOR .Yii::app()->theme->name;
	}

	public function stringClipping($str, $len)
	{
		return (strlen($str) > $len) ? trim (substr($str, 0, $len)) . '...' : $str;
	}
}