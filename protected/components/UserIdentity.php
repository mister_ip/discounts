<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;
	const ERROR_STATUS_NOTACTIV=3;
	/**
	 * Authenticates a user.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate() {
		$user = Users::model()->find('LOWER(login)=?', array(strtolower($this->username)));

		if (!$user){
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		}else if ($user->password !== md5 ($this->password)) {
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		}else {
			$this->_id=$user->id;
			$this->username = $user->login;
			$this->errorCode = self::ERROR_NONE;
		}
		return !$this->errorCode;
	}

	public function recoverauthenticate($user) {
		$this->username = $user->login;
	}

	public function getId(){
		return $this->_id;
	}
}