<?php

class WebUser extends CWebUser {
    
    private $model;
    
    public function getModel() {
        if(!isset($this->id)) $this->model = new Users;
        if($this->model === null)
             $this->model = Users::model()->findByPk($this->id, array('select' => 'role'));
        return $this->model;
    }
 
    public function __get($name) {
        try {
            return parent::__get($name);
        } catch (CException $e) {
            $m = $this->getModel();
            if(isset($m->{$name}))
                return $m->{$name};
            else throw $e;
        }
    }
 
    public function __set($name, $value) {
        try {
            return parent::__set($name, $value);
        } catch (CException $e) {
            $m = $this->getModel();
            $m->{$name} = $value;
        }
    }
 
    public function __call($name, $parameters) {
        try {
            return parent::__call($name, $parameters);  
        } catch (CException $e) {
            $m = $this->getModel();
            return call_user_func_array(array($m,$name), $parameters);
        }
    }
    
    public function isAdmin(){
        if (isset($this->isAdmin))
            return $this->getState('isAdmin');
        else
            return false;
    }
    
    public function isLoged(){
        return ($this->id != "");
    }
    function getRole() {
        if($user = $this->getModel()){
            return $user->role;
        }
    }
}
