<?php
class CardController extends PageController
{
	public function actionOrder()
	{
		$this->footer_slider = Companies::model()->findAll();
		$this->categories = Categories::model()->findAll();

		$this->render('CardOrderForm');
	}

	public function actionActivation()
	{
		$this->footer_slider = Companies::model()->findAll();
		$this->categories = Categories::model()->findAll();

		$this->render('CardActivationForm');
	}

	public function actionSend()
	{
		if (isset($_REQUEST['CARD'])) {

			$admin = Users::model()->find();

			$admin_email = 		$admin->email;
			$admin_message = 	"Фамилия: ".$_REQUEST['CARD']['surname']."\r\n".
				"Имя: ".$_REQUEST['CARD']['name']."\r\n".
				"Отчество: ".$_REQUEST['CARD']['patronymic']."\r\n".
				"Дата рождения: ".$_REQUEST['CARD']['birthday']."\r\n".
				"Пол: ".$_REQUEST['CARD']['sex']."\r\n".
				"Город: ".$_REQUEST['CARD']['city']."\r\n".
				"Email: ".$_REQUEST['CARD']['email']."\r\n".
				"Мобильный телефон: ".$_REQUEST['CARD']['phone_m']."\r\n".
				"Домашний телефон: ".$_REQUEST['CARD']['phone_h']."\r\n".
				"Адрес: ".$_REQUEST['CARD']['address']."\r\n";
			$this->sendEmail($admin_email, "Заказ карты", $admin_message);

			$user_email = 	$_REQUEST['CARD']['email'];
			$user_message = "Уважаемый ".$_REQUEST['CARD']['name'].", ваш заказ на получение карты принят!";
			$this->sendEmail($user_email, "Заказ", $user_message);
		}
		$this->redirect(array('/companies'));
	}

	public function actionActivate()
	{
		if (isset($_REQUEST['ACTIVATE'])) {

			$admin = Users::model()->find();

			$admin_email = 		$admin->email;
			$admin_message = 	"Номер карты: ".$_REQUEST['ACTIVATE']['number_card']."\r\n".
				"Фамилия: ".$_REQUEST['ACTIVATE']['surname']."\r\n".
				"Имя: ".$_REQUEST['ACTIVATE']['name']."\r\n".
				"Отчество: ".$_REQUEST['ACTIVATE']['patronymic']."\r\n".
				"Дата рождения: ".$_REQUEST['ACTIVATE']['birthday']."\r\n".
				"Пол: ".$_REQUEST['ACTIVATE']['sex']."\r\n".
				"Город: ".$_REQUEST['ACTIVATE']['city']."\r\n".
				"email: ".$_REQUEST['ACTIVATE']['email']."\r\n".
				"Мобильный телефон: ".$_REQUEST['ACTIVATE']['phone_m']."\r\n";
			$this->sendEmail($admin_email, "Активация карты", $admin_message);

			$user_email = 	$_REQUEST['ACTIVATE']['email'];
			$user_message = "Уважаемый ".$_REQUEST['ACTIVATE']['name'].", ваш запрос на активацию карты принят!";
			$this->sendEmail($user_email, "Активация", $user_message);
		}
		$this->redirect(array('/companies'));
	}

	private function sendEmail($to, $subject, $message)
	{
		$from = "discounts@example.com";
		$headers = "From:" . $from;
		mail($to,$subject,$message,$headers);
	}
}