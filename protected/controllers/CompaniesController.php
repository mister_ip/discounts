<?php
class CompaniesController extends PageController
{
	const PAGE_SIZE =9;

	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('save', 'delete'),
				'roles'=>array('admin'),
			),
			array('deny',
				'actions'=>array('save', 'delete')
			)
		);
	}

	public function actionIndex()
	{
		$this->footer_slider = Companies::model()->findAll();
		$this->categories = Categories::model()->findAll();

		isset($_REQUEST['category']) ? $selected_category = $_REQUEST['category'] : $selected_category = 0;

		$model = new Companies();
		$criteria = new CDbCriteria;
		$criteria->order = 'date_created DESC';
		$criteria->condition = $this->filter();
		$total = $model->count($criteria);
		$pages=new CPagination($total);
		$pages->pageSize = self::PAGE_SIZE;
		$pages->applyLimit($criteria);

		$categories = Categories::model()->findAll();
		$companies = Companies::model()->findAll($criteria);
		if ($companies)
			$this->render('index', array(	'categories'=>$categories,
											'selected_category'=>$selected_category,
											'companies'	=>$companies,
											'pages'=>$pages
										));
		else
			$this->render('empty', array('categories'=>$categories, 'selected_category'=>$selected_category));
	}
	public function actionProfile()
	{
		$this->footer_slider = Companies::model()->findAll();
		$this->categories = Categories::model()->findAll();

		if (isset($_REQUEST['id'])) {
			$company_id = (int)$_REQUEST['id'];
			$categories = Categories::model()->findAll();
			$company = Companies::model()->findByPk($company_id);
			$discounts = Discounts::model()->findAll(array("condition"=>"company_id = $company_id"));
			$this->render('profile', array( 'categories'=>$categories,
											'company'	=>$company,
											'discounts'	=>$discounts
										));
		}
		else {
			$this->redirect(array('/companies'));
		}
	}

	public function actionSave()
	{
		if (isset($_REQUEST['COMPANY']['id']))
			$company = Companies::model()->findByPk($_REQUEST['COMPANY']['id']);
		else
			$company = new Companies;

		$company->attributes = $_REQUEST['COMPANY'];
		$company->title = CHtml::encode($company->title);
		if (!$company->logo_path) $company->logo_path = "empty.png";
		$company->save(false);

		$this->moveFile($company->logo_path, $company->id);
		$this->redirect(array('/dcadmin/companies'));
	}

	public function actionDelete()
	{
		if (isset($_REQUEST['id']))
			Companies::model()->deleteByPk((int)$_REQUEST['id']);
		$this->redirect(array('/dcadmin/companies'));
	}

	private function filter()
	{
		$condition = "";
		if (isset($_REQUEST['category']))
			$condition .= "category_id = ".(int)$_REQUEST['category'];

		if (isset($_REQUEST['city'])) {
			$condition ? $condition .= " AND city = '{$_REQUEST['city']}'" : $condition .= "city = '{$_REQUEST['city']}'";
		}
		return $condition;
	}
}