<?php
class DcadminController extends AdminController
{
	public $defaultAction = 'companies';

	public function filters()
	{
		return array(
			'accessControl'
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'controllers'=>array('dcadmin'),
				'roles'=>array('admin')
			),
			array('deny',
				'controllers'=>array('dcadmin')
			)
		);
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionDiscounts()
	{
		$discounts = Discounts::model()->with()->findAll();
		$this->render('discounts', array('discounts'=>$discounts));
	}

	public function actionCompanies()
	{
		$companies = Companies::model()->with()->findAll();
		$this->render('companies', array('companies'=>$companies));
	}

	public function actionCategories()
	{
		$categories = Categories::model()->findAll();
		$this->render('categories', array('categories'=>$categories));
	}

	public function actionCompany()
	{
		if (isset($_REQUEST['id']))
			$company = Companies::model()->findByPk($_REQUEST['id']);
		else
			$company = '';
		$categories = Categories::model()->findAll();
		$this->render('company', array( 'categories'=>$categories, 'company'=>$company));
	}

	public function actionDiscount()
	{
		if (isset($_REQUEST['id']))
			$discount = Discounts::model()->findByPk($_REQUEST['id']);
		else
			$discount = '';
		$companies = Companies::model()->findAll();
		$this->render('discount',array( 'companies'=>$companies, 'discount'=>$discount));
	}

	public function actionCategory()
	{
		$this->render('category');
	}

	public function actionSettings()
	{
		$users = Users::model()->findAll();
		$this->render('settings', array('users'=>$users));
	}

	public function actionUpload()
	{
		$temp_folder = "temp";

		$path = YiiBase::getPathOfAlias('webroot') . "/storage/" .  $temp_folder;
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		$temp = explode(".", $_FILES["file"]["name"]);
		$extension = end($temp);
		if ((($_FILES["file"]["type"] == "image/gif")
				|| ($_FILES["file"]["type"] == "image/jpeg")
				|| ($_FILES["file"]["type"] == "image/jpg")
				|| ($_FILES["file"]["type"] == "image/pjpeg")
				|| ($_FILES["file"]["type"] == "image/x-png")
				|| ($_FILES["file"]["type"] == "image/png"))
			&& ($_FILES["file"]["size"] < 5000000)
			&& in_array($extension, $allowedExts))
		{
			if ($_FILES["file"]["error"] > 0)
			{
				$this->error = "Return Code: " . $_FILES["file"]["error"];
			}
			else
			{
				if (is_dir($path) || mkdir($path, 0777)) {
					move_uploaded_file($_FILES["file"]["tmp_name"], $path. "/" . $_FILES["file"]["name"]);
					$this->result['src'] = Yii::app()->getBaseUrl(true) . '/storage/'.$temp_folder.'/'.$_FILES["file"]["name"];
					$this->result['file'] = $_FILES["file"]["name"];
				}
			}
		}
		else
		{
			$this->error = "Invalid file";
		}
		$this->give_the_result($this->result);
	}

	public function actionDelcat()
	{
		Categories::model()->deleteByPk((int)$_REQUEST['id']);
		$this->result['del_id'] = $_REQUEST['id'];
		$this->give_the_result($this->result);
	}

	public function actionUpdatecat()
	{
		$cat = Categories::model()->findByPk((int)$_REQUEST['id']);
		$cat->title = $_REQUEST['title'];
		$cat->save(false);
	}

	public function  actionSavecat()
	{
		if(isset($_REQUEST['CATEGORY'])) {
			$cat = new Categories;
			$cat->title = $_REQUEST['CATEGORY']['title'];
			$cat->save(false);
		}
		$this->redirect(array('/dcadmin/category'));
	}
}