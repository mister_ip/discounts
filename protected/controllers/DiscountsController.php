<?php
class DiscountsController extends PageController
{
	const PAGE_SIZE = 20;
	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('save', 'delete'),
				'roles'=>array('admin'),
			),
			array('deny',
				'actions'=>array('save', 'delete')
			)
		);
	}

	public function actionIndex()
	{
		$this->footer_slider = Companies::model()->findAll();
		$this->categories = Categories::model()->findAll();

		isset($_REQUEST['category']) ? $selected_category = $_REQUEST['category'] : $selected_category = 0;

		$model = new Discounts();
		$criteria = new CDbCriteria;
		$criteria->order = 'start_date DESC';
		$total = $model->count($criteria);
		$pages=new CPagination($total);
		$pages->pageSize = self::PAGE_SIZE;
		$pages->applyLimit($criteria);

		$categories = Categories::model()->findAll();
		$discounts = Discounts::model()->with(array('company'=>$this->filter()))->findAll($criteria);
		if ($discounts)
			$this->render('index', array('selected_category'=>$selected_category,
										 'discounts'=>$discounts,
										 'pages'=>$pages
									));
		else
			$this->render('empty', array('categories'=>$categories, 'selected_category'=>$selected_category));
	}

	public function actionOrder()
	{
		$this->footer_slider = Companies::model()->findAll();
		$this->categories = Categories::model()->findAll();

		if(isset($_REQUEST['id'])) {
			$discount_id = (int) $_REQUEST['id'];
			$categories = Categories::model()->findAll();
			$discount = Discounts::model()->with('company')->findByPk($discount_id);
			$this->render('order', array('categories'=>$categories, 'discount'=>$discount));
		}
		else {
			$this->redirect(array('/discounts'));
		}
	}

	public function actionSend()
	{
		if (isset($_REQUEST['OrderForm'])) {
			$discount_id = (int)$_REQUEST['OrderForm']['discount_id'];
			$discount = Discounts::model()->with('company')->findByPk($discount_id);
			$admin = Users::model()->find();

			$admin_email = 		$admin->email;
			$admin_message = 	"имя: ".$_REQUEST['OrderForm']['user_name']."\r\n".
								"email: ".$_REQUEST['OrderForm']['email']."\r\n".
								"телефон: ".$_REQUEST['OrderForm']['phone']."\r\n".
								"сообщение: ".$_REQUEST['OrderForm']['comment']."\r\n";
			$this->sendEmail($admin_email, "Заказ", $admin_message);

			$user_email = 	$_REQUEST['OrderForm']['email'];
			$user_message = "Уважаемый ".$_REQUEST['OrderForm']['user_name'].", ваш заказ на скидку '".$discount->title."' принят!";
			$this->sendEmail($user_email, "Заказ", $user_message);
		}
		$this->redirect(array('/discounts'));
	}

	public function actionSave()
	{
		$discount_data = $_REQUEST['DISCOUNT'];
		$company = Companies::model()->findByPk($discount_data['company_id']);

		if (isset($discount_data['id']))
			$discount = Discounts::model()->findByPk($discount_data['id']);
		else {
			$discount = new Discounts;
			$company->amount_discounts += 1;
			$company->save(false);
		}

		$discount->attributes = $discount_data;
		$discount->category_id = $company->category_id;
		if (!$discount->img_path)
			$discount->img_path = $company->logo_path;
		$discount->save(false);
		$this->moveFile($discount->img_path, $discount->company_id);
		$this->redirect(array('/dcadmin/discounts'));
	}

	public function actionDelete()
	{
		if (isset($_REQUEST['id'])) {
			$company_id = Discounts::model()->findByPk((int)$_REQUEST['id'])->company_id;
			$company = Companies::model()->findByPk($company_id);
			$company->amount_discounts -= 1;
			$company->save(false);
			Discounts::model()->deleteByPk((int)$_REQUEST['id']);

		}
		$this->redirect(array('/dcadmin/discounts'));
	}

	private function filter()
	{
		$condition = "";
		if (isset($_REQUEST['category']))
			$condition .= "company.category_id = ".(int)$_REQUEST['category'];

		if (isset($_REQUEST['city'])) {
			$condition ? $condition .= " AND company.city = '{$_REQUEST['city']}'" : $condition .= "company.city = '{$_REQUEST['city']}'";
		}
		return array('condition'=>$condition);
	}

	private function sendEmail($to, $subject, $message)
	{
		$from = "discounts@example.com";
		$headers = "From:" . $from;
		mail($to,$subject,$message,$headers);
	}
}