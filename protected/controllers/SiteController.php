<?php

class SiteController extends PageController
{
	public function init()
	{
		parent::init();
		$this->footer_slider = Companies::model()->findAll();
		$this->categories = Categories::model()->findAll();

	}

	public function actionIndex()
	{
		$criteria = new CDbCriteria;
		$criteria->order = 'date_created DESC';
		$criteria->limit = 6;
		$companies = Companies::model()->findAll($criteria);

		$this->render('index', array('companies'=>$companies));
	}

	public function actionAbout()
	{
		$this->render('about');
	}

	public function actionDelivery()
	{
		$this->render('delivery');
	}

	public function actionContacts()
	{
		$this->render('contacts');
	}

	public function actionDescription()
	{
		$this->render('description_card');
	}

	public function actionAgreement()
	{
		$this->render('agreement');
	}

	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else {
				$this->render('error', array('error'=>$error));
			}
		}
	}
}