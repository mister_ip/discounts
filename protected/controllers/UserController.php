<?php
class UserController extends AdminController
{
	public $defaultAction = 'Login';

	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('save'),
				'roles'=>array('admin'),
			),
			array('deny',
				'actions'=>array('save'),
			)
		);
	}

	public function actionLogin()
	{
		if (!defined('CRYPT_BLOWFISH')||!CRYPT_BLOWFISH)
			throw new CHttpException(500,"This application requires that PHP was compiled with Blowfish support for crypt().");

		$model = new LoginForm;

		if(isset($_POST['LOGIN']))
		{
			$model->attributes=$_POST['LOGIN'];
			if($model->validate() && $model->login()) {
				$this->redirect(array('/dcadmin'));
			}
		}

		$this->layout = 'login';
		$this->render('login', array('baseUrl'=>Yii::app()->getBaseUrl(true)));
	}

	public function actionLogout()
	{
		Yii::app()->user->logout();
		Yii::app()->session->clear();
		$this->redirect(array('/user/login'));
	}

	public function actionSave()
	{
		if (isset($_REQUEST['USER'])) {
			$user_data = $_REQUEST['USER'];

			if (isset($user_data['id']))
				$user = Users::model()->findByPk($user_data['id']);
			else
				$user = new Users;
			$user->login = $user_data['login'];
			$user->email = $user_data['email'];
			if (trim($user_data['password']))
				$user->password = md5($user_data['password']);
			$user->save(false);
		}
		$this->redirect(array('/dcadmin/settings'));
	}
}