<?php

class LoginForm extends CFormModel
{
	public $login;
	public $password;
	public $remember;

	private $_identity;

	public function rules()
	{
		return array(
			// username and password are required
			array('login, password', 'required'),
			// remember needs to be a boolean
			//array('remember', 'boolean'),
			// password needs to be authenticated
			array('password', 'authenticate'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'remember'=>'Remember me next time',
		);
	}

	public function authenticate($attribute, $params)
	{
		if(!$this->hasErrors())
		{
			$this->_identity=new UserIdentity($this->login,$this->password);
			if(!$this->_identity->authenticate())
				$this->addError('password','Incorrect username or password.');
		}
	}

	public function login()
	{
		if($this->_identity===null) {
			$this->_identity=new UserIdentity($this->login,$this->password);
			$this->_identity->authenticate();
		}

		if($this->_identity->errorCode===UserIdentity::ERROR_NONE) {
			$duration = $this->remember ? 3600*24*30 : 0; // 30 days
			Yii::app()->user->login($this->_identity,$duration);
			return true;
		}
		else
			return false;
	}
}
