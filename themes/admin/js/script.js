var baseUrl;
baseUrl = "/";

$(function(){
	$('#discount-table').dataTable({
		"bJQueryUI": false,
		"sPaginationType": "full_numbers"
	} );
	$("#file").change(function(e) {
		saveFile();
	});
	//$('#company_save').on('click', saveCompany)
});

function saveCompany() {
	var company = {};
	company.title = 		$.trim($("#company_name").val());
	company.city = 			$.trim($("#company_city").val());
	company.category_id = 	$.trim($("#company_category").val());
	company.site = 			$.trim($("#company_site").val());
	company.phone = 		$.trim($("#company_phone").val());
	company.email = 		$.trim($("#company_email").val());
	company.address = 		$.trim($("#company_address").val());
	company.description = 	$.trim($("#company_description").val());
	company.details = 		$.trim($("#company_details").val());
	$.ajax({
		type: "POST",
		url: baseUrl + "companies/save",
		cache: false,
		data: {company: company},
		dataType: "json"
	}).done(function(res){
		console.log(res);
	});
}
function saveFile() {
	$('#image').empty().html('<img id="img" src="'+baseUrl+'images/ajax-loader.gif" />');
	var data = new FormData();
	data.append('file', $('#file')[0].files[0]);
	$.ajax({
		url: baseUrl + 'dcadmin/upload',
		dataType: "json",
		data: data,
		cache: false,
		contentType: false,
		processData: false,
		type: 'POST',
		success: function(res){
			if (res['e']){
				console.log("error", res['d']);
			}
			else {
				$('#company_logo').val(res['r']['file']);
				$('#discount_img').val(res['r']['file']);
				$('#image').empty().html('<img id="img" src="'+ res['r']['src'] +'"  width="210"/>');
			}
		}
	});
}
function deleteCategory(id) {
	console.log(id);
	$.ajax({
		url: baseUrl + 'dcadmin/delcat',
		dataType: "json",
		data: {id: id},
		cache: false,
		type: 'POST',
		success: function(res){
			if (res['e']){
				console.log("error", res['d']);
			}
			else {
				$($('#id' + id).parents()[1]).remove();
			}
		}
	});
}
function editCat(id){
	var $this = $('#id' + id),
		$parent = $($this.parents()[1]),
		$title = $parent.children(".title"),
		$button = $parent.children(".editing"),
		$input = $("<input type='text' class='form-control'/>"),
		$save = $("<button class='btn btn-success btn-xs'>save</button>"),
		$edit = $($this),
		text = $title.text();

	var send = function(){
		$.ajax({
			url: baseUrl + 'dcadmin/updatecat',
			dataType: "json",
			data: {id: id, title: $input.val()},
			cache: false,
			type: 'POST'
		});
	};

	$input.val(text);

	$input.keyup(function(e) {
		if (e.keyCode == 27) {
			$title.html(text);
			$edit.css({display: 'inline'});
			$save.css({display: 'none'});
		}
		if (e.keyCode == 13) {
			send();
			$title.html($input.val());
			$edit.css({display: 'inline'});
			$save.css({display: 'none'});
		}
	});

	$edit.css({display: 'none'});
	$button.append($save);

	$save.on('click', function(){
		send();
		$title.html($input.val());
		$edit.css({display: 'inline'});
		$save.css({display: 'none'});
	});
	$title.html($input);
	$input.focus();
}