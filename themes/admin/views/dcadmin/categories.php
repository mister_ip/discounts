<?php $this->beginClip('сontent'); ?>

	<div class="table-responsive well table-wrap">
		<table id="discount-table" class="table table-striped table-bordered table-hover">
			<thead>
			<tr>
				<th></th>
				<th>Категория</th>
				<th></th>
				<th></th>
			</tr>
			</thead>
			<tbody>
			<?php $i=1; foreach ($categories as $category) : ?>
				<tr>
					<td><?php echo $i++; ?></td>
					<td class="title"><?php echo $category->title; ?></td>
					<td class="editing"><a class="btn btn-info btn-xs edit" onclick="editCat(<?php echo $category->id; ?>)" id="id<?php echo $category->id; ?>">edit</a></td>
					<td><a class="btn btn-warning btn-xs" onclick="deleteCategory(<?php echo $category->id; ?>)">delete</a></td>
				</tr>
			<?php endforeach ?>
			</tbody>
		</table>
	</div>

<?php $this->endClip(); ?>