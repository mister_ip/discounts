<?php $this->beginClip('сontent'); ?>

	<div class="well">
		<form class="bs-example form-horizontal" action="<?php echo "{$this->baseUrl}/dcadmin/savecat";?>" method="POST">
			<fieldset>
				<legend>Данные о категории </legend>
				<div class="form-group">
					<label for="category_name" class="col-lg-2 control-label">Название</label>
					<div class="col-lg-10">
						<input type="text"
							   class="form-control"
							   id="category_name"
							   name="CATEGORY[title]"
							   placeholder="название категории"
							   required autofocus>
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-10 col-lg-offset-2">
						<button type="submit" class="btn btn-primary">Сохранить</button>
					</div>
				</div>
			</fieldset>
		</form>
	</div>

<?php $this->endClip(); ?>