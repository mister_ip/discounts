<?php $this->beginClip('сontent'); ?>

<div class="table-responsive well table-wrap">
	<table id="discount-table" class="table table-striped table-bordered table-hover">
		<thead>
		<tr>
			<th></th>
			<th>Название</th>
			<th>Категория</th>
			<th>Город</th>
			<th>Телефон</th>
			<th>email</th>
			<th></th>
			<th></th>
		</tr>
		</thead>
		<tbody>
		<?php $i=1; foreach ($companies as $company) : ?>
			<tr>
				<td><?php echo $i++; ?></td>
				<td><?php echo $company->title; ?></td>
				<td><?php echo $company->category->title; ?></td>
				<td><?php echo $company->city; ?></td>
				<td><?php echo $company->phone; ?></td>
				<td><?php echo $company->email; ?></td>
				<td>
					<a class="btn btn-info btn-xs" href="<?php echo "{$this->baseUrl}/dcadmin/company?id={$company->id}" ?>">edit</a>
				</td>
				<td>
					<a class="btn btn-warning btn-xs" href="<?php echo "{$this->baseUrl}/companies/delete?id={$company->id}" ?>">del</a>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
</div>

<?php $this->endClip(); ?>