<?php $this->beginClip('сontent'); ?>

<div class="well">
	<form class="bs-example form-horizontal" action="<?php echo "{$this->baseUrl}/companies/save";?>" method="POST">
		<fieldset>
			<legend>Данные о компании</legend>
			<div class="form-group">
				<label for="company_name" class="col-lg-2 control-label">Название</label>
				<div class="col-lg-10">
					<input type="text"
						   class="form-control"
						   id="company_name"
						   name="COMPANY[title]"
						   value="<?php if ($company) echo $company->title; ?>"
						   placeholder="название"
					required autofocus>
				</div>
			</div>
			<div class="form-group">
				<label for="company_city" class="col-lg-2 control-label">Город</label>
				<div class="col-lg-10">
					<input type="text"
						   class="form-control"
						   id="company_city"
						   name="COMPANY[city]"
						   value="<?php if ($company) echo $company->city; ?>"
						   placeholder="город"
					required>
				</div>
			</div>
			<div class="form-group">
				<label for="company_category" class="col-lg-2 control-label">Категория</label>
				<div class="col-lg-10">
					<select class="form-control" id="company_category" name="COMPANY[category_id]" required>
						<?php foreach ($categories as $category) {
							$s = "";
							if ($company && $company->category_id == $category->id) $s = "selected";
							echo "<option value={$category->id} $s>{$category->title}</option>";
						}
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="company_site" class="col-lg-2 control-label">Сайт</label>
				<div class="col-lg-10">
					<input type="text"
						   class="form-control"
						   id="company_site"
						   name="COMPANY[site]"
						   value="<?php if ($company) echo $company->site; ?>"
						   placeholder="url сайта"
					>
				</div>
			</div>
			<div class="form-group">
				<label for="company_phone" class="col-lg-2 control-label">Телефон</label>
				<div class="col-lg-10">
					<input type="text"
						   class="form-control"
						   id="company_phone"
						   name="COMPANY[phone]"
						   value="<?php if ($company) echo $company->phone; ?>"
						   placeholder="номер телефона"
					>
				</div>
			</div>
			<div class="form-group">
				<label for="company_email" class="col-lg-2 control-label">Email</label>
				<div class="col-lg-10">
					<input type="email"
						   class="form-control"
						   id="company_email"
						   name="COMPANY[email]"
						   value="<?php if ($company) echo $company->email; ?>"
						   placeholder="email"
					>
				</div>
			</div>
			<div class="form-group">
				<label for="company_address" class="col-lg-2 control-label">Адрес</label>
				<div class="col-lg-10">
					<input type="text"
						   class="form-control"
						   id="company_address"
						   name="COMPANY[address]"
						   value="<?php if ($company) echo $company->address; ?>"
						   placeholder="адрес"
					required>
					<span class="help-block">адрес в формате: город, улица, дом и т.д.</span>
				</div>
			</div>
			<div class="form-group">
				<label for="company_logo" class="col-lg-2 control-label">Логотип</label>
				<div class="col-lg-10">
					<div id="image">
						<?php if ($company) echo "<img id='img' src='{$this->baseUrl}/storage/{$company->id}/{$company->logo_path}' width='210'/>"; ?>
					</div>
					<input type="hidden"
						   class="form-control"
						   id="company_logo"
						   name="COMPANY[logo_path]"
						   value="<?php if ($company) echo $company->logo_path; ?>"
					required>
					<span class="btn btn-info btn-file">
						выбрать
						<input id="file" type="file"/>
					</span>
				</div>
			</div>
			<div class="form-group" class="col-lg-2 control-label">
				<label for="company_description" class="col-lg-2 control-label">Краткое описание</label>
				<div class="col-lg-10">
					<textarea class="form-control"
							  rows="3" id="company_description"
							  name="COMPANY[description]"
					><?php if ($company) echo $company->description; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-lg-2 control-label">Подробное описание</label>
				<div class="col-lg-10">
					<?php
					$details = '';
					if ($company) $details = $company->details;

					$this->widget('ImperaviRedactorWidget', array(
						'name' => 'COMPANY[details]',
						'id'=>'company_details',
						'value' => $details,
						'options' => array(
							'lang' => 'ru',
							'toolbar' => true,
							'minHeight' => '230',
							'placeholder' => 'Подробное (HTML) описание',
						),
					)); ?>
				</div>
			</div>
			<?php if ($company) echo "<input type='hidden' name='COMPANY[id]' value='{$company->id}'/>";?>
			<div class="form-group">
				<div class="col-lg-10 col-lg-offset-2">
					<button id="company_save" type="submit" class="btn btn-primary">Сохранить</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>

<?php $this->endClip(); ?>