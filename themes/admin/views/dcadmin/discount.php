<?php $this->beginClip('сontent'); ?>

	<div class="well">
		<form class="bs-example form-horizontal" action="<?php echo "{$this->baseUrl}/discounts/save";?>" method="POST">
			<fieldset>
				<legend>Данные об акции </legend>
				<div class="form-group">
					<label for="discount_name" class="col-lg-2 control-label">Название</label>
					<div class="col-lg-10">
						<input type="text"
							   class="form-control"
							   id="discount_name"
							   name="DISCOUNT[title]"
							   value="<?php if ($discount) echo $discount->title; ?>"
							   placeholder="название акции"
						required autofocus>
					</div>
				</div>
				<div class="form-group">
					<label for="discount_company" class="col-lg-2 control-label">Компания</label>
					<div class="col-lg-10">
						<select class="form-control"
								id="discount_company"
								name="DISCOUNT[company_id]"
							required>
							<?php foreach ($companies as $company) {
									$s = "";
									if ($discount && $discount->company_id == $company->id) $s = "selected";
									echo "<option value={$company->id} $s>{$company->title}</option>";
								}
							?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="discount_price" class="col-lg-2 control-label">Цена</label>
					<div class="col-lg-10">
						<input type="number"
							   class="form-control"
							   id="discount_price"
							   name="DISCOUNT[price]"
							   value="<?php if ($discount) echo $discount->price; ?>"
						required>
					</div>
				</div>
				<div class="form-group">
					<label for="discount_date_start" class="col-lg-2 control-label">Начало</label>
					<div class="col-lg-10">
						<?php
						if ($discount)
							$start_date = $discount->start_date;
						else
							$start_date = date('Y-m-d');

						//date picker widget
						$this->widget('ext.datepicker.RDatePicker',array(
							'name'=>'DISCOUNT[start_date]',
							'value'=>$start_date,
							'options' => array(
								'format' => 'yyyy-mm-dd',
								'viewformat' => 'yyyy-mm-dd',
								'placement' => 'right',
								'todayBtn'=>true,
								'class'=>'form-control',
							)
						));?>
					</div>
				</div>
				<div class="form-group">
					<label for="discount_date_end" class="col-lg-2 control-label">Окончание</label>
					<div class="col-lg-10">
						<?php
						if ($discount)
							$end_date = $discount->end_date;
						else
							$end_date = date('Y-m-d');

						//date picker widget
						$this->widget('ext.datepicker.RDatePicker',array(
							'name'=>'DISCOUNT[end_date]',
							'value'=>$end_date,
							'options' => array(
								'format' => 'yyyy-mm-dd',
								'viewformat' => 'yyyy-mm-dd',
								'placement' => 'right',
								'todayBtn'=>true,
								'class'=>'form-control',
							)
						));?>
					</div>
				</div>
				<div class="form-group">
					<label for="discount_img" class="col-lg-2 control-label">Логотип</label>
					<div class="col-lg-10">
						<div id="image">
							<?php if ($discount) echo "<img id='img' src='{$this->baseUrl}/storage/{$discount->company_id}/{$discount->img_path}' width='210'/>"; ?>
						</div>
						<input type="hidden"
							   class="form-control"
							   id="discount_img"
							   name="DISCOUNT[img_path]"
							   value="<?php if ($discount) echo $discount->img_path; ?>"
							   required>
					<span class="btn btn-info btn-file">
						выбрать
						<input id="file" type="file"/>
					</span>
					</div>
				</div>
				<div class="form-group">
					<label for="discount_description" class="col-lg-2 control-label">Краткое описание</label>
					<div class="col-lg-10">
						<textarea class="form-control"
								  rows="3"
								  id="discount_description"
								  name="DISCOUNT[description]"
						><?php if ($discount) echo $discount->description; ?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 control-label">Подробное описание</label>
					<div class="col-lg-10">
						<?php
						$details = '';
						if ($discount) $details = $discount->details;

						//redactor widget
						$this->widget('ImperaviRedactorWidget', array(
							'name' => 'DISCOUNT[details]',
							'value' => $details,
							'options' => array(
								'lang' => 'ru',
								'toolbar' => true,
								'minHeight' => '230',
								'placeholder' => 'Подробное (HTML) описание',
							),
						)); ?>
					</div>
				</div>
				<?php if ($discount) echo "<input type='hidden' name='DISCOUNT[id]' value='{$discount->id}'/>";?>
				<div class="form-group">
					<div class="col-lg-10 col-lg-offset-2">
						<button type="submit" class="btn btn-primary">Сохранить</button>
					</div>
				</div>
			</fieldset>
		</form>
	</div>

<?php $this->endClip(); ?>