<?php $this->beginClip('сontent'); ?>

<div class="table-responsive well table-wrap">
	<table id="discount-table" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th></th>
				<th>Название</th>
				<th>Компания</th>
				<th>Категория</th>
				<th>Начало</th>
				<th>Конец</th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
		<?php $i=1; foreach ($discounts as $discount) : ?>
			<tr>
				<td><?php echo $i++; ?></td>
				<td><?php echo $discount->title; ?></td>
				<td><?php echo $discount->company->title; ?></td>
				<td><?php echo $discount->category->title; ?></td>
				<td><?php echo $discount->start_date; ?></td>
				<td><?php echo $discount->end_date; ?></td>
				<td>
					<a class="btn btn-info btn-xs" href="<?php echo "{$this->baseUrl}/dcadmin/discount?id={$discount->id}" ?>">edit</a>
				</td>
				<td>
					<a class="btn btn-warning btn-xs" href="<?php echo "{$this->baseUrl}/discounts/delete?id={$discount->id}" ?>">del</a>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
</div>

<?php $this->endClip(); ?>