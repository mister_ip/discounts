<?php $this->beginClip('сontent'); ?>

<?php foreach ($users as $user) : ?>
<div class="well">
	<form class="bs-example form-horizontal" action="<?php echo "{$this->baseUrl}/user/save";?>" method="POST">
		<fieldset>
			<legend>Дополнительные настройки</legend>
			<div class="form-group">
				<label for="user_login" class="col-lg-2 control-label">Логин</label>
				<div class="col-lg-10">
					<input type="text"
						   class="form-control"
						   id="user_login"
						   name="USER[login]"
						   value="<?php if ($user) echo $user->login; ?>"
						   placeholder="login">
				</div>
			</div>
			<div class="form-group">
				<label for="user_email" class="col-lg-2 control-label">email</label>
				<div class="col-lg-10">
					<input type="email"
						   class="form-control"
						   id="user_email"
						   name="USER[email]"
						   value="<?php if ($user) echo $user->email; ?>"
						   placeholder="email">
				</div>
			</div>
			<div class="form-group">
				<label for="user_password" class="col-lg-2 control-label">Новый пароль</label>
				<div class="col-lg-10">
					<input type="password"
						   class="form-control"
						   id="user_password"
						   name="USER[password]"
						   value=""
						   placeholder="new password">
				</div>
			</div>
			<?php if ($user) echo "<input type='hidden' name='USER[id]' value='{$user->id}'/>";?>
			<div class="form-group">
				<div class="col-lg-10 col-lg-offset-2">
					<button type="submit" class="btn btn-primary">Сохранить</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>
<?php endforeach ?>

<?php $this->endClip(); ?>