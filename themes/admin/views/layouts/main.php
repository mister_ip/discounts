<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta charset="utf-8">
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
		<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	</head>
	<body>
		<div class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<a href="<?php echo $this->baseUrl ?>" class="navbar-brand">Discounts</a>
					<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<div class="navbar-collapse collapse" id="navbar-main">
					<ul class="nav navbar-nav">
						<li <?php if($this->action->id == 'companies') echo "class='active'";?>>
							<a href="<?php echo "{$this->baseUrl}/dcadmin/companies"; ?>">Компании</a>
						</li>
						<!--<li <?php if($this->action->id == 'discounts') echo "class='active'";?>>
							<a href="<?php //echo "{$this->baseUrl}/dcadmin/discounts" ?>">Акции</a>
						</li>-->
						<li <?php if($this->action->id == 'categories') echo "class='active'";?>>
							<a href="<?php echo "{$this->baseUrl}/dcadmin/categories" ?>">Категории</a>
						</li>
					</ul>

					<ul class="nav navbar-nav navbar-right">
						<li><a href="<?php echo "{$this->baseUrl}/user/logout" ?>">Выйти</a></li>
					</ul>
				</div>
			</div>
		</div>

		<div id="main" class="container">
			<div class="row">
				<div class="col-md-3 well">
					<ul class="nav nav-pills nav-stacked">
						<li <?php if($this->action->id == 'company') echo "class='active'";?>>
							<a href="<?php echo "{$this->baseUrl}/dcadmin/company"; ?>">Добавить компанию</a>
						</li>
						<!--<li <?php if($this->action->id == 'discount') echo "class='active'";?>>
							<a href="<?php //echo "{$this->baseUrl}/dcadmin/discount"; ?>">Добавить акцию</a>
						</li>-->
						<li <?php if($this->action->id == 'category') echo "class='active'";?>>
							<a href="<?php echo "{$this->baseUrl}/dcadmin/category"; ?>">Добавить категорию</a>
						</li>
						<li <?php if($this->action->id == 'settings') echo "class='active'";?>>
							<a href="<?php echo "{$this->baseUrl}/dcadmin/settings"; ?>">Настройки</a>
						</li>
					</ul>
				</div>
				<div class="col-md-9">
					<?php echo $this->clips['сontent']; ?>
				</div>
			</div>
		</div>
	</body>
</html>