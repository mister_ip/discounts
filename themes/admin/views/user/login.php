<?php $this->beginClip('сontent'); ?>

<div class="container">
	<form class="form-signin" action="<?php echo "{$this->baseUrl}/user/login"; ?>" method="POST">
		<h2 class="form-signin-heading">Please sign in</h2>
		<input type="text"
			   name="LOGIN[login]"
			   class="form-control input-block-level"
			   placeholder="Login"
		required autofocus>
		<input type="password"
			   name="LOGIN[password]"
			   class="form-control input-block-level"
			   placeholder="Password"
		required>
		<label class="checkbox">
			<input type="checkbox" name="LOGIN[remember]" value="remember-me"> Remember me
		</label>
		<button class="btn btn-large btn-primary" type="submit">Sign in</button>
	</form>
</div>

<?php $this->endClip(); ?>