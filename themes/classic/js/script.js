$(function(){
	$('#slider1').bxSlider({
		auto: true,
		pause: 3000,
		pager: false
	});

	$('#slider2').bxSlider({
		auto: true,
		pager: false,
		minSlides: 5,
		maxSlides: 5,
		moveSlides: 1,
		slideWidth: 230,
		slideMargin: 10
	});
});