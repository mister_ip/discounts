<?php
	$this->beginClip('сontent');
	$this->pageTitle=Yii::app()->name;
?>

<div class="left-col">
	<nav class="catalog">
		<ul class="menu vertical">
			<?php foreach ($categories as $category) : ?>
				<li class="<?php if($selected_category == $category->id) echo "active"?>">
					<a href="<?php echo "{$this->baseUrl}/companies?category={$category->id}"; ?>"><?php echo $category->title ?></a>
				</li>
			<?php endforeach ?>
		</ul>
	</nav>
</div>
<div class="right-col">
	<div class="companies">
		<?php foreach ($companies as $company) : ?>
			<a href="<?php echo $this->baseUrl.'/companies/profile?id='.$company->id; ?>" class="company">
				<span class="city"><?php echo $company->city; ?></span>
				<div class="company-logo"><img src="<?php echo "{$this->baseUrl}/storage/{$company->id}/{$company->logo_path}"; ?>" alt="<?php echo $company->title; ?>" width="190"/></div>
				<div class="company-name"><?php echo $company->title; ?></div>
				<div class="amount-discounts">
					<span>количество акций: </span>
					<span><?php echo $company->amount_discounts; ?></span>
				</div>
			</a>
		<?php endforeach ?>
	</div>
	<center>
		<?php $this->widget('CLinkPager',
			array(	'pages' => $pages,
				'header'=> '',
				'id'=>'link_pager',
				'maxButtonCount'=>5,
				'nextPageLabel'=>'следующая',
				'prevPageLabel'=>'предыдущая',
				'firstPageLabel'=>'первая',
				'lastPageLabel'=>'последняя',
				'cssFile'=>false
			));
		?>
	</center>
</div>

<?php $this->endClip(); ?>