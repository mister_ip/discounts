<?php
	$this->beginClip('сontent');
	$this->pageTitle=Yii::app()->name;
?>

<div class="left-col">
	<nav class="catalog">
		<ul class="menu vertical">
			<?php foreach ($categories as $category) : ?>
				<li>
					<a href="<?php echo "{$this->baseUrl}/discounts?category={$category->id}"; ?>"><?php echo $category->title ?></a>
				</li>
			<?php endforeach ?>
		</ul>
	</nav>
</div>
<div class="right-col">
	<div class="profile">
		<div class="company-head">
			<h1 class="company-title">“<?php echo $company->title; ?>”</h1>
			<div class="company-img">
				<img src="<?php echo "{$this->baseUrl}/storage/{$company->id}/{$company->logo_path}"; ?>" alt="<?php echo $company->title; ?>" width="190"/>
			</div>
		</div>
		<div class="company-body">
			<table class="company-table">
				<col width="15%">
				<col width="85%">
				<tbody>
				<tr>
					<td>
						<div class="table-head">Город</div>
					</td>
					<td><?php echo $company->city; ?></td>
				</tr>
				<tr>
					<td>
						<div class="table-head">Сайт</div>
					</td>
					<td><a href="http://<?php echo $company->site; ?>" target="_blank"><?php echo $company->site; ?></a></td>
				</tr>
				<tr>
					<td>
						<div class="table-head">Телефон</div>
					</td>
					<td><?php echo $company->phone; ?></td>
				</tr>
				<tr>
					<td>
						<div class="table-head">Email</div>
					</td>
					<td><?php echo $company->email; ?></td>
				</tr>
				<tr>
					<td>
						<div class="table-head">Адрес</div>
					</td>
					<td><?php echo $company->address; ?></td>
				</tr>
				<tr>
					<td>
						<div class="table-head">Описание</div>
					</td>
					<td><?php echo $company->description; ?></td>
				</tr>
				</tbody>
			</table>
			<div class="company-html">
				<?php echo $company->details; ?>
			</div>
			<div class="company-discounts">
				<h2 class="company-discounts-title">Акции компании:</h2>
				<?php if (count($discounts) == 0) echo "на данный момент у компании нет акций"; ?>
				<?php foreach ($discounts as $discount) : ?>
					<div class="discount">
						<div class="discount-logo">
							<img width="190" src="<?php echo "{$this->baseUrl}/storage/{$discount->company->id}/{$discount->img_path}"; ?>" alt="<?php echo $discount->title; ?>"/>
						</div>
						<div class="discount-text"><?php echo $this->stringClipping($discount->description, 80); ?></div>
						<div class="discount-price">
							<div class="price"><?php echo $discount->price ?> руб.</div>
							<a href="<?php echo "{$this->baseUrl}/discounts/order?id={$discount->id}"; ?>" class="button">Заказать</a>
						</div>
					</div>
				<?php endforeach ?>
			</div>
		</div>
		<div id="map" class="company-map"></div>
	</div>
</div>

<script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
<script>
	window.setTimeout(function(){
		var path = "<?php echo $company->address; ?>";
		ymaps.geocode(path, { results: 1 }).then(function (res) {
			var firstGeoObject = res.geoObjects.get(0),
				myMap = new ymaps.Map("map", {
					center: firstGeoObject.geometry.getCoordinates(),
					zoom: 16
				});
			myPlacemark = new ymaps.Placemark(firstGeoObject.geometry.getCoordinates(), {
					balloonContentHeader: "<?php echo $company->title; ?>",
					balloonContentBody: "<br/><?php echo $company->address; ?>",
					balloonContentFooter: "т. <?php echo $company->phone; ?>",
					hintContent: "<?php echo $company->title; ?>"
				},
				{
					hideIconOnBalloonOpen: true
				});

			myMap.geoObjects.add(myPlacemark);

			myMap.controls
				// Кнопка изменения масштаба.
				.add('zoomControl', { left: 5, top: 5 })
				// Список типов карты
				.add('typeSelector')
				// Стандартный набор кнопок
				.add('mapTools', { left: 35, top: 5 });

		});
	}, 1000);
</script>

<?php $this->endClip(); ?>