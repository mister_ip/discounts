<?php
$this->beginClip('сontent');
$this->pageTitle=Yii::app()->name;
?>

	<div class="left-col">
		<nav class="catalog">
			<ul class="menu vertical">
				<?php foreach ($categories as $category) : ?>
					<li class="<?php if($selected_category == $category->id) echo "active"?>">
						<a href="<?php echo "{$this->baseUrl}/discounts?category={$category->id}"; ?>"><?php echo $category->title ?></a>
					</li>
				<?php endforeach ?>
			</ul>
		</nav>
	</div>
	<div class="right-col">
		<p>в этой категории нет акций</p>
	</div>

<?php $this->endClip(); ?>