<?php
	$this->beginClip('сontent');
	$this->pageTitle=Yii::app()->name;
?>

<div class="left-col">
	<nav class="catalog">
		<ul class="menu vertical">
			<?php foreach ($categories as $category) : ?>
				<li class="<?php if($selected_category == $category->id) echo "active"?>">
					<a href="<?php echo "{$this->baseUrl}/discounts?category={$category->id}"; ?>"><?php echo $category->title ?></a>
				</li>
			<?php endforeach ?>
		</ul>
	</nav>
</div>
<div class="right-col">
	<div class="company-discounts">
		<?php foreach ($discounts as $discount) : ?>
			<div class="discount">
				<span class="city"><?php echo $discount->company->city; ?></span>
				<div class="discount-title"><?php echo $discount->company->title; ?></div>
				<div class="discount-logo">
					<img width="190" src="<?php echo "{$this->baseUrl}/storage/{$discount->company->id}/{$discount->img_path}"; ?>" alt="<?php echo $discount->title; ?>"/>
				</div>
				<div class="discount-text"><?php echo $this->stringClipping($discount->description, 80); ?></div>
				<div class="discount-price">
					<div class="price"><?php echo $discount->price ?> руб.</div>
					<a href="<?php echo "{$this->baseUrl}/discounts/order?id={$discount->id}"; ?>" class="button">Заказать</a>
				</div>
			</div>
		<?php endforeach ?>
	</div>
	<center>
		<?php $this->widget('CLinkPager',
			array(	'pages' => $pages,
					'header'=> '',
					'id'=>'link_pager',
					'maxButtonCount'=>5,
    				'nextPageLabel'=>'следующая',
					'prevPageLabel'=>'предыдущая',
					'firstPageLabel'=>'первая',
					'lastPageLabel'=>'последняя',
					'cssFile'=>false
			));
		?>
	</center>
</div>

<?php $this->endClip(); ?>