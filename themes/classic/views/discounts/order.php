<?php
	$this->beginClip('сontent');
	$this->pageTitle=Yii::app()->name;
?>

	<div class="left-col">
		<nav class="catalog">
			<ul class="menu vertical">
				<?php foreach ($categories as $category) : ?>
					<li>
						<a href="<?php echo "{$this->baseUrl}/discounts?category={$category->id}"; ?>"><?php echo $category->title ?></a>
					</li>
				<?php endforeach ?>
			</ul>
		</nav>
	</div>
	<div class="right-col">
		<div class="order">
			<div class="discount-order">
				<h1 class="title">акция компании “<?php echo $discount->company->title; ?>”</h1>
				<table>
					<tbody>
					<tr>
						<td>
							<div class="table-head">Акция</div>
						</td>
						<td><?php echo $discount->title; ?></td>
					</tr>
					<tr>
						<td>
							<div class="table-head">Окончание</div>
						</td>
						<td><?php echo $discount->end_date; ?></td>
					</tr>
					<tr>
						<td>
							<div class="table-head">Подробности</div>
						</td>
						<td><?php echo $discount->details; ?></td>
					</tr>
					<tr>
						<td>
							<div class="table-head">О компании</div>
						</td>
						<td><?php echo $discount->company->description; ?> (<a href="<?php echo "{$this->baseUrl}/companies/profile?id={$discount->company_id}"; ?>">больше</a>)</td>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="form-order">
				<p class="title">Для оформления заказа заполните поля формы обратной связи:</p>
				<form id="order" action="<?php echo "{$this->baseUrl}/discounts/send" ?>" method="POST">
					<input type="hidden" name="OrderForm[discount_id]"  value="<?php echo $discount->id; ?>"/>
					<div class="form-row">
						<label for="full-name">ФИО:</label>
						<input id="full-name" type="text" name="OrderForm[user_name]" required autofocus/>
					</div>
					<div class="form-row">
						<label for="email">Email:</label>
						<input id="email" type="email" name="OrderForm[email]" required/>
					</div>
					<div class="form-row">
						<label for="phone">Телефон:</label>
						<input id="phone" type="text" name="OrderForm[phone]" required/>
					</div>
					<div class="form-row">
						<label for="comment">Комментарий:</label>
						<textarea id="comment" name="OrderForm[comment]"></textarea>
					</div>
					<button class="button" type="submit">Отправить</button>
				</form>
			</div>
		</div>
	</div>

<?php $this->endClip(); ?>