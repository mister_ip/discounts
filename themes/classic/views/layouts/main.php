<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	</head>
	<body class="pattern1">
		<header class="pattern2">
			<div class="wrap">
				<div class="logo">
					<a href="<?php echo $this->baseUrl; ?>">
						<img src="<?php echo $this->baseUrl; ?>/themes/classic/images/logo.png" alt="MasterDiscount"/>
					</a>
				</div>
				<nav class="menu horizontal">
					<ul>
						<li <?php if($this->id == 'discounts') echo "class='active'";?>>
							<a href="<?php echo "{$this->baseUrl}/discounts"; ?>">Скидки</a>
						</li>
						<li <?php if($this->id == 'companies') echo "class='active indent'"; else echo "class='indent'";?>>
							<a href="<?php echo "{$this->baseUrl}/companies"; ?>">Компании</a>
						</li>
						<li <?php if($this->action->id == 'about') echo "class='active'";?>>
							<a href="<?php echo "{$this->baseUrl}/site/about"; ?>">О нас</a>
						</li>
						<li <?php if($this->action->id == 'delivery') echo "class='active'";?>>
							<a href="<?php echo "{$this->baseUrl}/site/delivery"; ?>">Доставка</a>
						</li>
					</ul>
				</nav>
			</div>
		</header>
		<section id="main" class="wrap">
			<?php echo $this->clips['сontent']; ?>
		</section>
		<footer>
			<span>Copyright © 2013 MasterDiscount</span>
		</footer>
	</body>
</html>