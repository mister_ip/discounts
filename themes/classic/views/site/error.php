<?php
$this->beginClip('сontent');
$this->pageTitle=Yii::app()->name;
?>

	<div class="left-col">
		<nav class="catalog">
			<ul class="menu vertical">
				<?php foreach ($categories as $category) : ?>
					<li>
						<a href="<?php echo "{$this->baseUrl}/discounts?category={$category->id}"; ?>"><?php echo $category->title ?></a>
					</li>
				<?php endforeach ?>
			</ul>
		</nav>
	</div>
	<div class="right-col">
		<p>Извините, запрашиваемый вами ресурс не найден</p>
	</div>

<?php $this->endClip(); ?>