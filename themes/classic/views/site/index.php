<?php
	$this->beginClip('сontent');
	$this->pageTitle=Yii::app()->name;
?>

<ul id="slider1">
	<li>
		<a href=""><img src="<?php echo $this->baseUrl; ?>/images/slider/1.png"></a>
	</li>
	<li>
		<a href=""><img src="<?php echo $this->baseUrl; ?>/images/slider/2.png"></a>
	</li>
</ul>
<div class="banner">
	<div class="banner-1 left">
		<a href="<?php echo $this->baseUrl; ?>/discounts"><img src="<?php echo $this->baseUrl; ?>/themes/classic/images/banner-1.png" alt=""/></a>
	</div>
	<div class="banner-2 left">
		<a href=""><img src="<?php echo $this->baseUrl; ?>/themes/classic/images/banner-2.png" alt=""/></a>
	</div>
</div>
<ul id="slider2">
	<?php foreach ($companies as $company) : ?>
		<li>
			<a href="<?php echo "{$this->baseUrl}/companies/profile?id={$company->id}"; ?>">
				<img src="<?php echo "{$this->baseUrl}/storage/{$company->id}/{$company->logo_path}"; ?>">
			</a>
		</li>
	<?php endforeach ?>
</ul>
<?php $this->endClip(); ?>