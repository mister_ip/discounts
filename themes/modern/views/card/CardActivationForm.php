<?php
$this->beginClip('сontent');
$this->pageTitle=Yii::app()->name;
?>

	<div class="oPosBlock row-fluid sidebar">
		<div class="oMainBlock">
			<div class="container">
				<div class="oPos sidebar-1 span3 visible-phone visible-tablet visible-desktop">
					<?php  include($this->path . "/views/components/catalog.php"); ?>
				</div>
				<div class="oContentBlock span9 visible-phone visible-tablet visible-desktop">
					<div class="oPos maincontent">
						<div class="component">
							<div class="contact">
								<div class="ot-content">
									<div class="page-header">
										<h2>Активация карты</h2>
									</div>
									<div>
										<form class="form-horizontal" action="<?php echo $this->baseUrl; ?>/card/activate" method="POST">
											<fieldset>
												<legend>Если Вы уже получили карту, но при этом не заполнили анкету, просим Вас пройти регистрацию!</legend>
												<div class="control-group">
													<label class="control-label" for="number_card">Номер карты<span class="star">&nbsp;*</span></label>
													<div class="controls">
														<input type="text" id="number_card" name="ACTIVATE[number_card]" placeholder="Номер карты" required>
													</div>
												</div>
												<div class="control-group">
													<label class="control-label" for="surname">Фамилия<span class="star">&nbsp;*</span></label>
													<div class="controls">
														<input type="text" id="surname" name="ACTIVATE[surname]" placeholder="Фамилия" required>
													</div>
												</div>
												<div class="control-group">
													<label class="control-label" for="name">Имя<span class="star">&nbsp;*</span></label>
													<div class="controls">
														<input type="text" id="name" name="ACTIVATE[name]" placeholder="Имя" required>
													</div>
												</div>
												<div class="control-group">
													<label class="control-label" for="patronymic">Отчество<span class="star">&nbsp;*</span></label>
													<div class="controls">
														<input type="text" id="patronymic" name="ACTIVATE[patronymic]" placeholder="Отчество" required>
													</div>
												</div>
												<div class="control-group">
													<label class="control-label" for="email">Электронная почта<span class="star">&nbsp;*</span></label>
													<div class="controls">
														<input type="email" id="email" name="ACTIVATE[email]" placeholder="email" required>
													</div>
												</div>
												<div class="control-group">
													<label class="control-label" for="phone_m">Мобильный телефон<span class="star">&nbsp;*</span></label>
													<div class="controls">
														<input type="text" id="phone_m" name="ACTIVATE[phone_m]" placeholder="Мобильный телефон" required>
													</div>
												</div>
											</fieldset>
											<fieldset>
												<legend>Дополнительная информация</legend>
												<div class="control-group">
													<label class="control-label" for="birthday">Дата рождения<span class="star">&nbsp;*</span></label>
													<div class="controls">
														<?php $this->widget('ext.datepicker.RDatePicker',array(
															'name'=>'ACTIVATE[birthday]',
															'value'=>'',
															'options' => array(
																'format' => 'yyyy-mm-dd',
																'viewformat' => 'yyyy-mm-dd',
																'placement' => 'right',
																'todayBtn'=>true,
																'class'=>'form-control',
															)
														));?>
													</div>
												</div>
												<div class="control-group">
													<label class="control-label" for="sex">Пол<span class="star">&nbsp;*</span></label>
													<div class="controls">
														<label class="radio">
															<input type="radio" name="ACTIVATE[sex]" value="1">мужской
														</label>
														<label class="radio">
															<input type="radio" name="ACTIVATE[sex]" value="0">женский
														</label>
													</div>
												</div>
												<div class="control-group">
													<label class="control-label" for="city">Город<span class="star">&nbsp;*</span></label>
													<div class="controls">
														<select name="ACTIVATE[city]" id="city">
															<option value="1">Москва</option>
															<option value="1">Киев</option>
															<option value="1">Ростов</option>
														</select>
													</div>
												</div>
												<div class="control-group">
													<div class="controls">
														<label class="checkbox">
															<input type="checkbox" name="ACTIVATE[delivery]" value="1" required>Я принимаю условия соглашения
														</label>
														<a href="<?php echo $this->baseUrl; ?>/site/agreement" target="_blank">пользовательское соглашение</a>
													</div>
												</div>
											</fieldset>
											<fieldset>
												<div class="form-actions">
													<button type="submit" class="btn btn-warning">Активировать</button>
												</div>
											</fieldset>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php $this->endClip(); ?>