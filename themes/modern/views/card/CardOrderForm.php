<?php
$this->beginClip('сontent');
$this->pageTitle=Yii::app()->name;
?>

<div class="oPosBlock row-fluid sidebar">
	<div class="oMainBlock">
		<div class="container">
			<div class="oPos sidebar-1 span3 visible-phone visible-tablet visible-desktop">
				<?php  include($this->path . "/views/components/catalog.php"); ?>
			</div>
			<div class="oContentBlock span9 visible-phone visible-tablet visible-desktop">
				<div class="oPos maincontent">
					<div class="component">
						<div class="contact">
							<div class="ot-content">
								<div class="page-header">
									<h2>Заказ карты</h2>
								</div>
								<div>
									<form class="form-horizontal" action="<?php echo $this->baseUrl; ?>/card/send" method="POST">
										<fieldset>
											<legend>Покупатель:</legend>
											<div class="control-group">
												<label class="control-label" for="surname">Фамилия</label>
												<div class="controls">
													<input type="text" id="surname" name="CARD[surname]" placeholder="Фамилия">
												</div>
											</div>
											<div class="control-group">
												<label class="control-label" for="name">Имя<span class="star">&nbsp;*</span></label>
												<div class="controls">
													<input type="text" id="name" name="CARD[name]" placeholder="Имя" required>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label" for="patronymic">Отчество</label>
												<div class="controls">
													<input type="text" id="patronymic" name="CARD[patronymic]" placeholder="Отчество">
												</div>
											</div>
											<div class="control-group">
												<label class="control-label" for="birthday">Дата рождения<span class="star">&nbsp;*</span></label>
												<div class="controls">
													<?php $this->widget('ext.datepicker.RDatePicker',array(
														'name'=>'CARD[birthday]',
														'value'=>'',
														'options' => array(
															'format' => 'yyyy-mm-dd',
															'viewformat' => 'yyyy-mm-dd',
															'placement' => 'right',
															'todayBtn'=>true,
															'class'=>'form-control',
														)
													));?>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label" for="sex">Пол<span class="star">&nbsp;*</span></label>
												<div class="controls">
													<label class="radio">
														<input type="radio" name="CARD[sex]" value="1">мужской
													</label>
													<label class="radio">
														<input type="radio" name="CARD[sex]" value="0">женский
													</label>
												</div>
											</div>
										</fieldset>
										<fieldset>
											<legend>Почтовый адрес:</legend>
											<div class="control-group">
												<label class="control-label" for="city">Город<span class="star">&nbsp;*</span></label>
												<div class="controls">
													<select name="CARD[city]" id="city">
														<option value="1">Москва</option>
														<option value="1">Киев</option>
														<option value="1">Ростов</option>
													</select>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label" for="address">Адресс<span class="star">&nbsp;*</span></label>
												<div class="controls">
													<input type="text" id="patronymic" name="CARD[address]" placeholder="Адресс" required>
												</div>
											</div>
										</fieldset>
										<fieldset>
											<legend>Контактные данные:</legend>
											<div class="control-group">
												<label class="control-label" for="phone_h">Домашний телефон</label>
												<div class="controls">
													<input type="text" id="phone_h" name="CARD[phone_h]" placeholder="Домашний телефон">
												</div>
											</div>
											<div class="control-group">
												<label class="control-label" for="phone_m">Мобильный телефон<span class="star">&nbsp;*</span></label>
												<div class="controls">
													<input type="text" id="phone_m" name="CARD[phone_m]" placeholder="Мобильный телефон" required>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label" for="email">Электронная почта<span class="star">&nbsp;*</span></label>
												<div class="controls">
													<input type="email" id="email" name="CARD[email]" placeholder="email" required>
												</div>
											</div>
											<div class="control-group">
												<div class="controls">
													<label class="checkbox">
														<input type="checkbox" name="CARD[delivery]" value="1" checked>Я согласен на использование моих данных для получения e-mail рассылки
													</label>
												</div>
											</div>
										</fieldset>
										<fieldset>
											<div class="form-actions">
												<button type="submit" class="btn btn-primary">Заказать</button>
											</div>
										</fieldset>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->endClip(); ?>