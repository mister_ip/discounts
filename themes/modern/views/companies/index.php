<?php
	$this->beginClip('сontent');
	$this->pageTitle=Yii::app()->name;
?>
<div class="oPosBlock row-fluid sidebar">
	<div class="oMainBlock">
		<div class="container">
			<div class="oPos sidebar-1 span3 visible-phone visible-tablet visible-desktop">
				<?php  include($this->path . "/views/components/catalog.php"); ?>
			</div>
			<div class="oContentBlock span9 visible-phone visible-tablet visible-desktop">
				<?php  include($this->path . "/views/components/companies_list.php"); ?>
				<div class="vm-pagination pagination pagination-mini pagination-centered">
					<?php $this->widget('CLinkPager',
						array(	'pages' => $pages,
							'header'=> '',
							'id'=>'link_pager',
							'maxButtonCount'=>5,
							'nextPageLabel'=>'следующая',
							'prevPageLabel'=>'предыдущая',
							'firstPageLabel'=>'первая',
							'lastPageLabel'=>'последняя',
							'cssFile'=>false
						));
					?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->endClip(); ?>