<?php
	$this->beginClip('сontent');
	$this->pageTitle=Yii::app()->name;
?>

<div class="oPosBlock row-fluid sidebar">
	<div class="oMainBlock">
		<div class="container">
			<div class="oPos sidebar-1 span3 visible-phone visible-tablet visible-desktop">
				<?php  include($this->path . "/views/components/catalog.php"); ?>
			</div>
			<div class="oContentBlock span9 visible-phone visible-tablet visible-desktop">
				<div class="oPos maincontent">
					<div class="component">
						<div class="productdetails-view productdetails">
							<div class="ot-content">
								<div class="product-preview">
									<div class="span5">
										<div class="product_img_31">
											<div class="main-image">
												<div class="tab-content">
													<div class="tab-pane active">
														<a rel="vm-additional-images" href="">
															<img src="<?php echo "{$this->baseUrl}/storage/{$company->id}/{$company->logo_path}"; ?>" alt="<?php echo $company->title; ?>"/>
														</a>
													</div>
												</div>
												<div class="clear"></div>
											</div>
										</div>
									</div>
									<div class="span7">
										<div class="product-name">
											<h1><?php echo $company->title; ?></h1>
										</div>
										<div class="clear"></div>
										<div class="spacer-buy-area">
											<div class="addtocart-area">
												<table class="company-table">
													<col width="25%">
													<col width="75%">
													<tbody>
														<tr>
															<td><div class="table-head">Город</div></td>
															<td class="company-info"><?php echo $company->city; ?></td>
														</tr>
														<?php if (trim($company->site)): ?>
														<tr>
															<td><div class="table-head">Сайт</div></td>
															<td class="company-info"><a href="http://<?php echo $company->site; ?>" target="_blank"><?php echo $company->site; ?></a></td>
														</tr>
														<?php endif; ?>
														<?php if (trim($company->phone)): ?>
														<tr>
															<td><div class="table-head">Телефон</div></td>
															<td class="company-info"><?php echo $company->phone; ?></td>
														</tr>
														<?php endif; ?>
														<?php if (trim($company->email)): ?>
														<tr>
															<td><div class="table-head">Email</div></td>
															<td class="company-info"><?php echo $company->email; ?></td>
														</tr>
														<?php endif; ?>
														<tr>
															<td><div class="table-head">Адрес</div></td>
															<td class="company-info"><?php echo $company->address; ?></td>
														</tr>
														<?php if (trim($company->description)): ?>
														<tr>
															<td><div class="table-head">Описание</div></td>
															<td class="company-info"><?php echo $company->description; ?></td>
														</tr>
														<?php endif; ?>
													</tbody>
												</table>
											</div>
											<div class="clear"></div>
										</div>
									</div>
									<div class="clear"></div>
								</div>
								<div class="product-details">
									<?php echo $company->details; ?>
								</div>
								<div id="map" class="company-map"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
<script>
	window.setTimeout(function(){
		var path = "<?php echo $company->address; ?>";
		ymaps.geocode(path, { results: 1 }).then(function (res) {
			var firstGeoObject = res.geoObjects.get(0),
				myMap = new ymaps.Map("map", {
					center: firstGeoObject.geometry.getCoordinates(),
					zoom: 16
				});
			myPlacemark = new ymaps.Placemark(firstGeoObject.geometry.getCoordinates(), {
					balloonContentHeader: "<?php echo $company->title; ?>",
					balloonContentBody: "<br/><?php echo $company->address; ?>",
					balloonContentFooter: "т. <?php echo $company->phone; ?>",
					hintContent: "<?php echo $company->title; ?>"
				},
				{
					hideIconOnBalloonOpen: true
				});

			myMap.geoObjects.add(myPlacemark);

			myMap.controls
				// Кнопка изменения масштаба.
				.add('zoomControl', { left: 5, top: 5 })
				// Список типов карты
				.add('typeSelector')
				// Стандартный набор кнопок
				.add('mapTools', { left: 35, top: 5 });

		});
	}, 1000);
</script>

<?php $this->endClip(); ?>