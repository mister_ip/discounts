<div class="ot-mod-outer standard module">
	<div class="ot-mod-inner clearfix">
		<h3 class="mod-title">
			<span>Категории</span>
		</h3>
		<div class="mod-content clearfix">
			<ul class="VMmenu menu nav">
				<?php foreach ($this->categories as $category) : ?>
					<li class="level0  VmClose">
						<a href="<?php echo "{$this->baseUrl}/companies?category={$category->id}"; ?>"><?php echo $category->title ?></a>
					</li>
				<?php endforeach ?>
			</ul>
		</div>
	</div>
</div>