<div class="oPosBlock row-fluid content-top">
	<div class="oPos content-top-1 span12 visible-phone visible-tablet visible-desktop">
		<div class="ot-mod-outer standard module">
			<div class="ot-mod-inner clearfix">
				<div class="mod-content clearfix">
					<div class="vmgroup">
						<div class="vmgroup-i ot-items">
							<?php
							$i=0;
							foreach ($companies as $company) {
								if($i % 3  == 0) echo "<div class='item'>";
								?>
								<div class="product ot-product span4 <?php if($i % 3  == 0) echo 'first'; if($i % 3  == 2) echo 'last';?>">
									<div class="spacer">
										<div class="product-image center">
											<a href="<?php echo $this->baseUrl.'/companies/profile?id='.$company->id; ?>">
												<img src="<?php echo "{$this->baseUrl}/storage/{$company->id}/{$company->logo_path}"; ?>" alt=""/>
											</a>
											<div class="clear"></div>
										</div>
										<div class="clear"></div>
										<!--<span class="product-discount">30%</span>-->
										<div class="product-detail">
											<a class="product-name" href="<?php echo $this->baseUrl.'/companies/profile?id='.$company->id; ?>"><?php echo $company->title; ?></a>
											<div class="clear"></div>
											<div class="product-s-desc">
												<p class="product_s_desc">
													<?php echo $this->stringClipping($company->description, 80); ?>
												</p>
											</div>
											<div class="clear"></div>
										</div>
										<div class="product-addtocartbar">
											<div class="product-price span6">
												<div class="PricesalesPrice" style="display: block; padding-top: 3px;">
													<span class="PricesalesPrice"><?php echo $company->city; ?></span>
												</div>
											</div>
											<div class="product-addtocart span6">
												<a class="product-details" href="<?php echo $this->baseUrl.'/companies/profile?id='.$company->id; ?>">Подробнее</a>
											</div>
											<div class="clear"></div>
										</div>
									</div>
									<div class="clear"></div>
								</div>
								<?php
								if($i % 3  == 2) echo "</div>";
								$i++;
							}; ?>
						</div>
						<div class='clear'></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>