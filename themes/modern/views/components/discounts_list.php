<div class="company-discounts">
	<?php foreach ($discounts as $discount) : ?>
		<div class="discount">
			<span class="city"><?php echo $discount->company->city; ?></span>
			<div class="discount-title"><?php echo $discount->company->title; ?></div>
			<div class="discount-logo">
				<img width="190" src="<?php echo "{$this->baseUrl}/storage/{$discount->company->id}/{$discount->img_path}"; ?>" alt="<?php echo $discount->title; ?>"/>
			</div>
			<div class="discount-text"><?php echo $this->stringClipping($discount->description, 80); ?></div>
			<div class="discount-price">
				<div class="price"><?php echo $discount->price ?> руб.</div>
				<a href="<?php echo "{$this->baseUrl}/discounts/order?id={$discount->id}"; ?>" class="button">Заказать</a>
			</div>
		</div>
	<?php endforeach ?>
</div>