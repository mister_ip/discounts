<li class="ot-menu-item level1 last notColumn <?php if($this->id == 'site' && $this->action->id == 'index') echo 'active';?>">
	<div>
		<a class="item-link level1" href="<?php echo "{$this->baseUrl}"; ?>">
			<span class="item-text">Главная</span>
		</a>
	</div>
</li>
<li class="ot-menu-item level1 last notColumn <?php if($this->id == 'companies') echo 'active';?>">
	<div>
		<a class="item-link level1" href="<?php echo "{$this->baseUrl}/companies"; ?>">
			<span class="item-text">Компании</span>
		</a>
	</div>
</li>
<li class="ot-menu-item level1 last notColumn <?php if($this->action->id == 'description') echo 'active';?>">
	<div>
		<a class="item-link level1" href="<?php echo "{$this->baseUrl}/site/description"; ?>">
			<span class="item-text">Описание карты</span>
		</a>
	</div>
</li>
<li class="ot-menu-item level1 last notColumn <?php if($this->id == 'card' && $this->action->id == 'order') echo 'active';?>">
	<div>
		<a class="item-link level1" href="<?php echo "{$this->baseUrl}/card/order"; ?>">
			<span class="item-text">Заказать карту</span>
		</a>
	</div>
</li>
<li class="ot-menu-item level1 last notColumn <?php if($this->id == 'card' && $this->action->id == 'activation') echo 'active';?>">
	<div>
		<a class="item-link level1" href="<?php echo "{$this->baseUrl}/card/activation"; ?>">
			<span class="item-text">Активировать карту</span>
		</a>
	</div>
</li>