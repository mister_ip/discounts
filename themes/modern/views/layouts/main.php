<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	</head>
	<body class="oBody ltr homepage frontpage">
		<div class="body-bg">
			<div class="wrapper">
				<header class="container">
					<div class="oPosBlock row-fluid top1">
						<div class="oPos top1-1 span4 visible-phone visible-tablet visible-desktop">
							<div class="ot-mod-outer standard module">
								<div class="ot-mod-inner clearfix">
									<div class="mod-content clearfix">
										<div class="custom">
											<div class="ot_logo">
												<a href="<?php echo $this->baseUrl; ?>">
													<img src="<?php echo $this->baseUrl; ?>/themes/modern/images/logo.png" border="0" alt="">
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="oPos top1-2 span8 visible-phone visible-tablet visible-desktop">
							<div class="ot-mod-outer standard module Clearfix">
								<div class="ot-mod-inner clearfix">
									<div class="mod-content clearfix">
										<div class="custom Clearfix">
											<div style="height: 80px;"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="ot-mod-outer standard module floatleft">
								<div class="ot-mod-inner clearfix">
									<div class="mod-content clearfix">
										<div class="custom floatleft">
											<div class="custom_top">
												<img src="<?php echo $this->baseUrl; ?>/themes/modern/images/custom_top.png" border="0"/>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="ot-mod-outer standard module custom_top_menu">
								<div class="ot-mod-inner clearfix">
									<div class="mod-content clearfix">
										<ul class="menu">
											<li><a href="<?php echo "{$this->baseUrl}/site/about"; ?>">О нас</a></li>
											<li><span class="separator">|</span></li>
											<li><a href="<?php echo "{$this->baseUrl}/site/delivery"; ?>">Доставка</a></li>
											<li><span class="separator">|</span></li>
											<li><a href="<?php echo "{$this->baseUrl}/site/contacts"; ?>">Контакты</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="oPosBlock row-fluid top2">
						<div class="oPos top2-1 span12 visible-phone visible-tablet visible-desktop">
							<div class="ot-mod-outer standard module mainmenu">
								<div class="ot-mod-inner clearfix">
									<div class="mod-content clearfix">
										<div class="otmenu-wrapper otmenu-desk-wrapper visible-desktop">
											<div class="otmenu-wrapper-i">
												<ul class="ot-menu ot-dropdown-100 menu">
													<?php include($this->path . "/views/components/main_menu.php"); ?>
												</ul>
											</div>
										</div>
										<div class="otmenu-wrapper otmenu-mobile-wrapper hidden-desktop">
											<div class="otmenu-wrapper-i">
												<a class="btn btn-navbar" data-togle="collapse" data-target=".ot-sliding-100">
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
												</a>
												<div class="clearfix">
													<ul class="ot-menu ot-sliding-100 menu in" style="height: auto;">
														<?php include($this->path . "/views/components/main_menu.php"); ?>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<nav class="menu horizontal">
					</nav>
				</header>
				<section id="main" class="wrap">
					<?php echo $this->clips['сontent']; ?>
				</section>
				<footer class="oFooterBlock">
					<div class="oFooterBlock1">
						<div class="container">
							<div class="oPosBlock row-fluid footer1">
								<ul id="slider2">
									<?php foreach ($this->footer_slider as $company) : ?>
										<li>
											<a href="<?php echo "{$this->baseUrl}/companies/profile?id={$company->id}"; ?>">
												<img src="<?php echo "{$this->baseUrl}/storage/{$company->id}/{$company->logo_path}"; ?>">
											</a>
										</li>
									<?php endforeach ?>
								</ul>
							</div>
						</div>
					</div>
					<div class="oFooterBlock2">
						<div class="container">
							<div class="oPosBlock row-fluid footer2">
								<div class="oPos footer2-1 span6 visible-phone visible-tablet visible-desktop">
									<div class="ot-mod-outer standard module">
										<div class="ot-mod-inner clearfix">
											<div class="mod-content clearfix">
												<div class="custom">
													<div class="copyright">
														<span>Copyright © 2013 MasterDiscount</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="oPos footer2-2 span6 visible-phone visible-tablet visible-desktop">
									<div class="ot-mod-outer standard module floatright">
										<div class="ot-mod-inner clearfix">
											<div class="mod-content clearfix">
												<ul class="menu">
													<li class="item-551" <?php if($this->id == 'index') echo "class='active'";?>>
														<a href="<?php echo $this->baseUrl; ?>">Главная</a>
													</li>
													<li class="item-555">
														<span class="separator">|</span>
													</li>
													<li class="item-551" <?php if($this->id == 'companies') echo "class='active'";?>>
														<a href="<?php echo "{$this->baseUrl}/companies"; ?>">Компании</a>
													</li>
													<li class="item-555">
														<span class="separator">|</span>
													</li>
													<li class="item-551" <?php if($this->id == 'card' && $this->action->id == 'order') echo "class='active'";?>>
														<a href="<?php echo "{$this->baseUrl}/card/order"; ?>">Заказать карту</a>
													</li>
													<li class="item-555">
														<span class="separator">|</span>
													</li>
													<li class="item-551" <?php if($this->action->id == 'about') echo "class='active'";?>>
														<a class="item-link level1" href="<?php echo "{$this->baseUrl}/site/about"; ?>">О нас</a>
													</li>
													<li class="item-555">
														<span class="separator">|</span>
													</li>
													<li class="item-551" <?php if($this->action->id == 'delivery') echo "class='active'";?>>
														<a class="item-link level1" href="<?php echo "{$this->baseUrl}/site/delivery"; ?>">Доставка</a>
													</li>
													<li class="item-555">
														<span class="separator">|</span>
													</li>
													<li class="item-551" <?php if($this->action->id == 'contacts') echo "class='active'";?>>
														<a class="item-link level1" href="<?php echo "{$this->baseUrl}/site/contacts"; ?>">Контакты</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</footer>
			</div>
		</div>
	</body>
</html>