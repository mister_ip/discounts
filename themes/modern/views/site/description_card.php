<?php
$this->beginClip('сontent');
$this->pageTitle=Yii::app()->name;
?>

	<div class="oPosBlock row-fluid sidebar">
		<div class="oMainBlock">
			<div class="container">
				<div class="oPos sidebar-1 span3 visible-phone visible-tablet visible-desktop">
					<?php  include($this->path . "/views/components/catalog.php"); ?>
				</div>
				<div class="oContentBlock span9 visible-phone visible-tablet visible-desktop">
					<div class="oPos maincontent">
						<div class="component">
							<div class="contact">
								<div class="ot-content">
									<div class="page-header">
										<h2>Как получить карту?</h2>
									</div>
									<div class="description-item">
										<h3>Как получить единую клубную карту «Master Discount»?</h3>
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi consequatur cum debitis deserunt, dignissimos dolor ducimus eius et expedita harum iusto, nisi obcaecati pariatur possimus quis sapiente tenetur totam!
										</p>
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequuntur, culpa cupiditate error eum expedita fugit, odit officia omnis placeat quibusdam sapiente soluta voluptate. Autem dicta magnam quisquam quos voluptates?
										</p>
									</div>
									<div class="description-item">
										<h3>В настоящее время клубную карту «Master Discount» можно получить следующим образом: </h3>
										<ol>
											<li>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium nam odio sapiente veniam voluptas. Aliquam atque dolorem eaque exercitationem fuga iste natus optio, placeat quas quos, repudiandae similique velit veritatis?</p>
												<p><a href="<?php echo "{$this->baseUrl}/card/order"; ?>">On-line заявка на получение клубной карты «Master Discount»</a></p>
											</li>
											<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam explicabo nisi odio quaerat quibusdam quod. Accusantium cupiditate dolore earum mollitia numquam, odit sed sunt tempora velit. Architecto dolorem modi quia!</li>
										</ol>
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium alias aperiam asperiores atque, cupiditate deserunt dolores doloribus dolorum eveniet fuga impedit, inventore nesciunt nostrum nulla pariatur quaerat quis similique velit?
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php $this->endClip(); ?>