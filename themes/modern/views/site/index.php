<?php
	$this->beginClip('сontent');
	$this->pageTitle=Yii::app()->name;
?>
<div class="oMiddleBlock">
	<div class="oMiddleBlock1">
		<div class="container">
			<div class="oPosBlock row-fluid middle1">
				<div class="oPos middle1-1 span12 visible-phone visible-tablet visible-desktop">
					<ul id="slider1">
						<li>
							<a href="">
								<img src="<?php echo $this->baseUrl; ?>/images/slider/1.png">
							</a>
						</li>
						<li>
							<a href="">
								<img src="<?php echo $this->baseUrl; ?>/images/slider/2.png">
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="oMiddleBlock2">
		<div class="container">
			<div class="oPosBlock row-fluid middle2">
				<div class="oPos middle2-1 span12 visible-phone visible-tablet visible-desktop">
					<div class="ot-mod-outer standard module">
						<div class="ot-mod-inner clearfix">
							<div class="mod-content clearfix">
								<div class="custom">
									<div class="btn-group span12 no-space">
										<a href="<?php echo $this->baseUrl; ?>/companies" class="btn btn-large span8">
											<span class="custom_title">Мастер Скидок предлагает</span>
											<span class="custom_style2 custom_desc">каталог скидок</span>
										</a>
										<a class="btn btn-large span4">
											<span class="custom_title">Скидки на все</span>
											<span class="custom_style2 custom_desc">70%</span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="oMainBlock">
		<div class="container">
			<div class="oPosBlock row-fluid sidebar">
				<div class="oPos sidebar-1 span3 visible-phone visible-tablet visible-desktop">
					<?php  include($this->path . "/views/components/catalog.php"); ?>
				</div>

				<div class="oContentBlock span9 visible-phone visible-tablet visible-desktop">
					<?php  include($this->path . "/views/components/companies_list.php"); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->endClip(); ?>